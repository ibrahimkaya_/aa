﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circle2 : MonoBehaviour {

    Rigidbody2D physic;
    public float speed;
    bool coll = false;
    GameObject gamemaster;

	void Start () {
        physic = GetComponent<Rigidbody2D>();
        gamemaster = GameObject.FindGameObjectWithTag("gameControl");
	}

    void FixedUpdate()
    {
        if (coll==false)
            physic.MovePosition(physic.position + Vector2.up * speed * Time.deltaTime);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="rollcircle")
        {
            transform.SetParent(collision.transform);
            coll = true;
        }

        if (collision.tag == "circle")
        {
            gamemaster.GetComponent<gameControl>().gameOver();

        }
    }
}

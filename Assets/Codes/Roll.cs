﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Roll : MonoBehaviour {

    public float speed = 150;
    public float time = 0;
    public int rotate;
    public float delay;
    int lvl;

     void Start()
    {
        randomSpeed();
        random();
        lvl = int.Parse(SceneManager.GetActiveScene().name);
    }

    void Update()
    {

        if (lvl == 4 || lvl == 7 || lvl == 12 || lvl == 16 || lvl == 18 || lvl == 22 || lvl == 23 || lvl == 25)
        {
            time += Time.deltaTime;
            if (time < delay)
            {
                if (rotate == 0)
                {
                    transform.Rotate(0, 0, speed * Time.deltaTime);
                }
                else
                {
                    transform.Rotate(0, 0, speed * Time.deltaTime * (-1));
                }
            }
            else
            {
                random(); time = 0;
                randomSpeed();
            }
        }
        else
            transform.Rotate(0, 0, speed * Time.deltaTime);
    }

    void randomSpeed()
    {

        if (int.Parse(SceneManager.GetActiveScene().name) < 2)
            speed = Random.Range(100, 125);
        else if (int.Parse(SceneManager.GetActiveScene().name) < 3)
            speed = Random.Range(125, 150);
        else if (int.Parse(SceneManager.GetActiveScene().name) < 5)
            speed = Random.Range(150, 175);
        else if (int.Parse(SceneManager.GetActiveScene().name) < 8)
            speed = Random.Range(175, 200);
        else if (int.Parse(SceneManager.GetActiveScene().name) < 11)
            speed = Random.Range(200, 250);
        else if (int.Parse(SceneManager.GetActiveScene().name) < 13)
            speed = Random.Range(250, 275);
        else if (int.Parse(SceneManager.GetActiveScene().name) < 16)
            speed = Random.Range(275,300);
        else if (int.Parse(SceneManager.GetActiveScene().name) < 18)
            speed = Random.Range(300,325);
        else if (int.Parse(SceneManager.GetActiveScene().name) < 20)
            speed = Random.Range(325,350);
        else if (int.Parse(SceneManager.GetActiveScene().name) < 22)
            speed = Random.Range(350, 325);
        else if (int.Parse(SceneManager.GetActiveScene().name) == 25)
            speed = Random.Range(350,450);
    }

    void random()
    {
        if (int.Parse(SceneManager.GetActiveScene().name) == 4)
        {
            delay = Random.Range(10, 12);
            int _rotate = Random.Range(0, 2);
            if (_rotate == rotate)
                rotate = Random.Range(0, 2);
            else
                rotate = _rotate;
        }

        else if (int.Parse(SceneManager.GetActiveScene().name) == 7)
        {
            delay = Random.Range(8.12f, 10.14f);
            int _rotate = Random.Range(0, 2);
            if (_rotate == rotate)
                rotate = Random.Range(0, 2);
            else
                rotate = _rotate;
        }

        else if (int.Parse(SceneManager.GetActiveScene().name) == 12)
        {
            delay = Random.Range(2, 3);
            int _rotate = Random.Range(0, 2);
            if (_rotate == rotate)
                rotate = Random.Range(0, 2);
            else
                rotate = _rotate;
        }

        else if (int.Parse(SceneManager.GetActiveScene().name) == 16)
        {
            delay = Random.Range(7, 9);
            int _rotate = Random.Range(0, 2);
            if (_rotate == rotate)
                rotate = Random.Range(0, 2);
            else
                rotate = _rotate;
        }

        else if (int.Parse(SceneManager.GetActiveScene().name) == 18)
        {
            delay = Random.Range(11, 13);
            int _rotate = Random.Range(0, 2);
            if (_rotate == rotate)
                rotate = Random.Range(0, 2);
            else
                rotate = _rotate;
        }

        else if (int.Parse(SceneManager.GetActiveScene().name) == 22)
        {
            delay = Random.Range(5, 7);
            int _rotate = Random.Range(0, 2);
            if (_rotate == rotate)
                rotate = Random.Range(0, 2);
            else
                rotate = _rotate;
        }

        else if (int.Parse(SceneManager.GetActiveScene().name) == 23)
        {
            delay = Random.Range(4, 8);
            int _rotate = Random.Range(0, 2);
            if (_rotate == rotate)
                rotate = Random.Range(0, 2);
            else
                rotate = _rotate;
        }

        else if (int.Parse(SceneManager.GetActiveScene().name) == 25)
        {
            delay = Random.Range(2, 3);
            int _rotate = Random.Range(0, 2);
            if (_rotate == rotate)
                rotate = Random.Range(0, 2);
            else
                rotate = _rotate;
        }
        else
            rotate = Random.Range(0, 2);



    }
}



//transform.Rotate(0, 0, speed * Time.deltaTime);        
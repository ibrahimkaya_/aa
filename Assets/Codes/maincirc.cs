﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class maincirc : MonoBehaviour {

    public GameObject circle2;
    GameObject gameMast;

    void Start () {

        gameMast = GameObject.FindGameObjectWithTag("gameControl");
		
	}
	
	void Update () {

        if (Input.GetButtonDown("Fire1"))
        {
            makeCircle();
        }
		
	}

    void makeCircle()
    {

        Instantiate(circle2, transform.position, transform.rotation);
        gameMast.GetComponent<gameControl>().textShow();

    }
}


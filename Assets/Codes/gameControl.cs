﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameControl : MonoBehaviour {

    GameObject rollingCirc;
    GameObject circle;
    public Animator gameover;
    public Animator gameWin;
    public Text levelTxT;
    public Text crc1;
    public Text crc2;
    public Text crc3;
    public int lvlcirclenum;    
    GameObject circle1;
    GameObject circle2;

    void Start () {

        levelcirclenum();
        circle = GameObject.FindGameObjectWithTag("maincircle");
        rollingCirc = GameObject.FindGameObjectWithTag("rollcircle");
        levelTxT.text = SceneManager.GetActiveScene().name;
        circle1 = GameObject.FindGameObjectWithTag("maincircle2");
        circle2 = GameObject.FindGameObjectWithTag("maincircle3");
        

        if (lvlcirclenum < 2)
            crc1.text = lvlcirclenum.ToString();
        else if (lvlcirclenum < 3)
        {
            crc1.text = lvlcirclenum.ToString();
            crc2.text = (lvlcirclenum - 1).ToString();
        }
        else
        {
            crc1.text = lvlcirclenum.ToString();
            crc2.text = (lvlcirclenum - 1).ToString();
            crc3.text = (lvlcirclenum - 2).ToString();
        }
	}
	
    public void textShow()
    {
        lvlcirclenum--;

        if (lvlcirclenum == 0) 
        {
            crc1.text = null;
            circle.GetComponent<SpriteRenderer>().enabled = false;
            circle.GetComponent<maincirc>().enabled = false;
            StartCoroutine(_nextlevel());


        }

        else if (lvlcirclenum < 2)
        {
            crc1.text = lvlcirclenum.ToString();
            crc2.text = null;
            crc3.text = null;
            circle1.GetComponent<SpriteRenderer>().enabled = false;
            circle2.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (lvlcirclenum < 3)
        {
            crc1.text = lvlcirclenum.ToString();
            crc2.text = (lvlcirclenum - 1).ToString();
            crc3.text = null;
            circle2.GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            crc1.text = lvlcirclenum.ToString();
            crc2.text = (lvlcirclenum - 1).ToString();
            crc3.text = (lvlcirclenum - 2).ToString();
        }


    }

    public void gameOver()
    {
        StartCoroutine(_gameOver());
    }

    IEnumerator _gameOver()
    {
        circle.GetComponent<maincirc>().enabled = false;
        rollingCirc.GetComponent<Roll>().enabled = false;
        gameover.SetTrigger("gameOver");
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("menu");
    }

    IEnumerator _nextlevel()
    {

        gameWin.SetTrigger("gameWin");
        yield return new WaitForSeconds(3);
        int scene = int.Parse(SceneManager.GetActiveScene().name);
        SceneManager.LoadScene((scene + 1).ToString());
        
    }
    
    void levelcirclenum()
    {
        if (int.Parse(SceneManager.GetActiveScene().name) == 1)
            lvlcirclenum = 5;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 2)
            lvlcirclenum = 7;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 3)
            lvlcirclenum = 12;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 4)
            lvlcirclenum = 15;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 5)
            lvlcirclenum = 22;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 6)
            lvlcirclenum = 23;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 7)
            lvlcirclenum = 25;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 8)
            lvlcirclenum = 27;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 9)
            lvlcirclenum = 24;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 10)
            lvlcirclenum = 18;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 11)
            lvlcirclenum = 17;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 12)
            lvlcirclenum = 22;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 13)
            lvlcirclenum = 30;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 14)
            lvlcirclenum = 27;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 15)
            lvlcirclenum = 15;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 16)
            lvlcirclenum = 12;        
        else if (int.Parse(SceneManager.GetActiveScene().name) == 17)
            lvlcirclenum = 15;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 18)
            lvlcirclenum = 18;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 19)
            lvlcirclenum = 20;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 20)
            lvlcirclenum = 18;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 21)
            lvlcirclenum = 26;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 22)
            lvlcirclenum = 32;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 23)
            lvlcirclenum = 20;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 24)
            lvlcirclenum = 18;
        else if (int.Parse(SceneManager.GetActiveScene().name) == 25)
            lvlcirclenum = 30;



    }
}


